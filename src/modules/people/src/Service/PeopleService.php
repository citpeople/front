<?php

namespace Drupal\people\Service;

use Drupal\people\Model\ListItemData;
use Drupal\people\Model\PersonProfileData;
use GuzzleHttp\Client as HttpClient;

class PeopleService
{

  /**
   * @var HttpClient
   */
  protected $client = null;

  /**
   * @var string
   */
  protected $apiPath = 'http://35.237.196.137:8080/api/v1';

  /**
   * PeopleService constructor.
   * @param HttpClient $client
   */
  public function __construct()
  {
    $this->client = new HttpClient();
  }


  /**
   * @param string $user_login
   * @return PersonProfileData
   * @throws \Exception
   */
  public function fetchUserBylogin($user_login)
  {
    $response = $this->client->get($this->apiPath . '/person/' . $user_login);

    if ($response->getStatusCode() !== 200) {
      throw new \Exception($this->t('Failed to fetch @user_login data, reason: @reason', [
        '@user_login' => $user_login,
        '@reason' => $response->getReasonPhrase(),
      ]));
    }

    return new PersonProfileData((string)$response->getBody());
  }

  /**
   * @param string $q
   * @return ListItemData[]
   * @throws \Exception
   */
  public function fetchUserList($q)
  {
    $response = $this->client->get($this->apiPath . '/person', [
      'query' => compact('q')
    ]);

    if ($response->getStatusCode() !== 200) {
      throw new \Exception($this->t('Failed to fetch data for the term @term, reason: @reason', [
        '@term' => $q,
        '@reason' => $response->getReasonPhrase(),
      ]));
    }

    $return = [];
    $response = json_decode($response->getBody(), true);
    foreach ($response as $item) {
      $return[] = new ListItemData($item);
    }

    return $return;
  }

}
