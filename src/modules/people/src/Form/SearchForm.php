<?php

namespace Drupal\people\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class SearchForm extends FormBase {
  /**
   * Build the simple form.
   *
   * A build form method constructs an array that defines how markup and
   * other form elements are included in an HTML form.
   *
   * @param array $form
   *   Default form array structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Object containing current form state.
   *
   * @return array
   *   The render array defining the elements of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];
    $form['#attached'] = [
      'library' => 'people/people'
    ];
    $form['head'] = [
      '#type' => 'item',
      '#markup' => '<h1 class="people-search">' . $this->t("People<br>Get to know CI&T's") . '</h1>'
    ];
    $form['q'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Who'),
      //'#description' => $this->t('Who do you want to find?'),
      '#default_value' => '',
    ];
    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Search'),
      ],
    ];
    return $form;
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    // should verify if user exists. Maybe request user data and cache it locally?
    $form_state->setRedirect('people.profile', ['user_login' => $form['q']['#value']]);
  }

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId()
  {
    return 'people_module_search_form';
  }

}

