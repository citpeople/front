<?php
namespace Drupal\people\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'People' Block
 *
 * @Block(
 *   id = "people_block",
 *   admin_label = @Translation("People block"),
 * )
 */
class PeopleBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (!empty($config['people_block_settings'])) {
      $name = $config['people_block_settings'];
    }
    else {
      $name = $this->t('to no one');
    }
    return array(
      '#markup' => $this->t(
          '@name!',
          array(
            '@name' => $name,
          )
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $default_config = \Drupal::config('people.settings');
    $config = $this->getConfiguration();

    $form['people_block_settings'] = array (
      '#type' => 'textfield',
      '#title' => $this->t('Who'),
      '#description' => $this->t('Who do you want to find?'),
      '#default_value' => isset($config['people_block_settings']) ? $config['people_block_settings'] : $default_config->get('people.id'),
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfigurationValue('people_block_settings', $form_state->getValue('people_block_settings'));
  }
}
