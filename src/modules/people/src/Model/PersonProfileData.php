<?php

namespace Drupal\people\Model;

/**
 * @property string $photo
 * @property string $fullName
 * @property string $role
 * @property string $email
 * @property string $localization
 * @property string $coach
 * @property string $pdm
 * @property string $bp
 * @property string $area
 * @property string $contact
 * @property string $bio
 * @property string[] $currentClients
 * @property string[] $previousClients
 * @property array $awards
 * @property array $awardsImages
 * @property array $skillRadar
 * @property string $myteam
 * @property string[] $socialLinks
 */
class PersonProfileData extends BaseModel
{

  /**
   * @return array
   */
  public function getDrupalElementDefinition()
  {
    $element = [
      '#attached' => [
        'library' => 'people/people'
      ],
    ];

    foreach ($this->data as $name => $item) {
      if (!is_string($item)) {
        $item = json_encode($item);
      }
      $element[$name] = [
        '#markup' => '<div>' . $this->t($item) . '</div>',
      ];
    }

    return $element;
  }

}
