<?php

namespace Drupal\people\Model;


use Drupal\Core\StringTranslation\StringTranslationTrait;

abstract class BaseModel
{

  use StringTranslationTrait;

  /**
   * @var array Original data from JSON
   */
  protected $data = [];

  /**
   * @param array|string $data
   */
  public function __construct($data = [])
  {
    if (is_string($data)) {
      $data = json_decode($data, true);
    } else if (!is_array($data)) {
      $data = (array) $data;
    }

    $this->data = $data;
  }

  /**
   * @param string $name
   * @return mixed|null
   */
  public function __get($name)
  {
    $data = null;
    if (isset($this->data[$name])) {
      $data = $this->data[$name];
    }

    return $data;
  }

  /**
   * @return array
   */
  abstract public function getDrupalElementDefinition();

}
