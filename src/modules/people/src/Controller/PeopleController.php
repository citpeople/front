<?php

namespace Drupal\people\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\people\Form\SearchForm;
use Drupal\people\Service\PeopleService;
use GuzzleHttp\Client as HttpClient;

class PeopleController extends ControllerBase {

  /**
   *
   * @return array
   */
  public function searchForm() {
    $form = [];
    $form['form'] = $this->formBuilder()->getForm(SearchForm::class);
    $form['form']['top-right'] = [
      '#markup' => '<div class="detail-top-right"></div>'
    ];
    $form['form']['bottom-left'] = [
      '#markup' => '<div class="detail-bottom-left"></div>'
    ];
    return $form;
  }


  /**
   *
   * @param string $user_login
   * @return array
   */
  public function profile($user_login) {

    try {
      $service = new PeopleService();
      $user = $service->fetchUserBylogin($user_login);
      $element = $user->getDrupalElementDefinition();
    } catch (\Exception $ex) {
      $element['exception'] = [
        '#markup' => $this->t($ex->getMessage()),
      ];
    }
    $element['#title'] = 'Title';

    return $element;
  }

  /**
   * This should be in that other place, and it will be. Just wait and see. :)
   * @param string $query
   * @param string $type
   * @return array
   */
  public function request($query = '', $type = 'search') {
    return [];
  }
}
